#define N 4


typedef struct {
    int vet[N];
    int topo;
}pilha;

void iniciaPilha(pilha *pilha1){
    pilha1->topo = 0;
}

int vaziaPilha(pilha *pilha1){
    if(pilha1->topo == 0)
        return (1);
    else
        return 0;
}

int cheiaPilha(pilha *pilha1){
    if(pilha1->topo == (N))
        return 1;
    else
        return 0;
}

int pop(pilha *pilha1){
    pilha1->topo--;
    return (pilha1->vet[pilha1->topo]);
}

int push(pilha *pilha1, int x){
    pilha1->vet[pilha1->topo] = x;
    pilha1->topo++;
    return (pilha1->topo);
}

void exibePilha(pilha *pilha1){
    int i;
    for(i = 0; i < pilha1->topo; i++)
        printf("|%d|\n",pilha1->vet[i]);
}