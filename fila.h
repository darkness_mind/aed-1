#ifndef FILA_H_INCLUDED
#define FILA_H_INCLUDED
#define N 4
#define ERROFILACHEIA -1
#define ERROFILAVAZIA -2

typedef struct{
    int vetor[N];
    int ini;
    int fim;
    int cont;
}Fila;

void iniFila(Fila *f){
    f->ini = 0;
    f->fim = 0;
    f->cont = 0;
}

int vaziaFila(Fila *f){
    if (f->cont == 0){
        return 1;
    }
    else{
        return 0;
    }
}

int cheiaFila(Fila *f){
    if (f->cont == N-1){
        return 1;
    }
    else {
        return 0;
    }
}

int insereFila(Fila *f, int valor){
    if (cheiaFila(f)){
        return (ERROFILACHEIA);
    }
    else{
        f->vetor[f->fim] = valor;
        f->cont++;
        if (f->fim == N-1){
            f->fim = 0;
        }
        else {
            f->fim++;
        }
        return (f->fim);
    }
}

int removeFila(Fila *f){
    int x;
    if(vaziaFila(f)){
        return ERROFILAVAZIA;
    }
    else{
        x = f->vetor[f->ini];
        f->cont--;
        if (f->ini == N-1){
            f->ini = 0;
        }
        else{
            f->ini++;
        }
    return x;
    }
}

void exibir(Fila *f){
    int i, j;
    j = f->ini;
    for(i = 0; i < f->cont; i++){
        printf("%d\n", f->vetor[j]);
        if(j == N-1){
            j = 0;
        }
        else {
            j++;
        }
    }
}

#endif // FILA_H_INCLUDED
